# YEAST Dataset

Introduction
----

**YEAST - YET Another Surgical Training Dataset**. 
The acquired dataset consists of 42 trials of the same suturing task performed by a single expert user. The expert is right hand dominant and she has more than 50 hours of experience with da Vinci surgical robotic system. Trials can be divided into two macro-classes: the first 20 using a different phantom position than the last 20 in which the phantom is turned 45 degrees clockwise. Trials 21 and 42 do not contain suturing task but a procedure useful for spatial calibration. Only PSM1_position_cartesian_current.csv and PSM2_position_cartesian_current.csv are spatially calibrated.

The following abbreviations will be used below:
- ECM: Endoscopic Camera Manipulator; 
- PSM: Patient Side Manipulator; 
- MTML: Master Tool Manipulator Left; 
- MTMR: Master Tool Manipulator Right.

Structure of the files contained in the dataset
----
Each trial  consists of 10 Comma Separated Value (CSV) files that captured raw kinematic data (time, position and orientation) and a video that reproduces the entire surgical scene captured by a stereo endoscopic camera. Files contained in the dataset are listed below:
- *(ECM|PSM1|PSM2|MTML|MTMR)_position_Cartesian_current.csv*: 
they contain temporal information about position and orientation in the Cartesian 3D space of the ECM, PSM1, PSM2, MTML and MTMR respectively.
- *(ECM|PSM1|PSM2|MTML|MTMR)_state_joint_current.csv*: they contain temporal information about position, velocity and effort of each joint for ECM, PSM1, PSM2, MTML and MTMR respectively. Joints are: 
    *outer-yaw*, 
    *outer-pitch*, 
    *insertion*, 
    *outer-roll*,
    *outer-wrist-pitch*,
    *outer-wrist-yaw*. 
   


Contact and cite us
----
If you are using the dataset please cite the following paper:

M. Bombieri, D. Dall'Alba, S. Ramesh, G. Menegozzo, C. Schneider and P. Fiorini, "Joints-Space Metrics for Automatic Robotic Surgical Gestures Classification," 2020 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS), Las Vegas, NV, USA, 2020, pp. 3061-3066, doi: 10.1109/IROS45743.2020.9341094.

Please do not hesitate to contact us:
marco.bombieri_01@univr.it

